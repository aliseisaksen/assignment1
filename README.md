# README #

Alise Isaksen
ami325

Explain the architecture of your caching client

I have chosen to implement HTTP caching using primarily Java’s URLConnection class. My caching client caches data related to the input URL and recalls it so long as the cached data remains fresh. If it is discovered that data in my cache is stale upon a new request, my program will deliver content straight from the source and store the new content back to the cache. Otherwise, if the cache entries are determined to be fresh upon another request to the same host/resource pair is made, then the cached data will be served to output.

To help with the caching mechanism of my program, the user-specified URL is refined using my urlCleaner() method. The purpose of this step is to remove any characters in the URL that may be unsafe to use in a filename. I use the clean URL plus a “.txt” extension as the name for the file where I keep the header and body data in cache.

In addition to the .txt file containing the data, I also create a .ser file, also using the clean URL as the filename, such that every <cleanURL>.txt will have a <cleanURL>.ser file. The .ser file contains a serialized map of certain header data I will need when determining the freshness of the datafile stored in cache. This way, if a repeat request for the same URL occurs at a later time, my program will search through the cache directory for the two files (*.txt and *.ser) containing the related data.

If the *.txt/*.ser files are not found in the cache directory, this means that there is no cache entry for these resources; either the resource has never before been requested or the Max-Age/Expires values prevent the program from caching the data.


Given the unpredictable nature of dynamic content, I believe my program would consider it ‘uncacheable.’ Even if dynamic content claimed to be valid for some period of time, it is not known whether the given expiry date is always going to be viable. It is possible for a resource to be cached for very short periods, given that it has some max-age value (albeit a short one). In these cases, my caching client can check to verify whether the resource has been modified, cutting down bandwidth and network traffic my some small amount.