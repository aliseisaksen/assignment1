import java.net.*;
import java.io.*;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Author: Alise Isaksen
 * 
 */

public class CachingHTTPClient {
	
	public static void main(String args[]) {
				
		if (args.length < 1) {
			System.out.println("Usage:");
			System.out.println("java TestUrlConnection <url>");
			System.exit(0);
		}

		URL url = null;
		
		try {
			url = new URL(args[0]);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		assert url != null;
		try {
			
			// Create safe-to-use filename Strings from URL
	        String cleanURL = urlCleaner(args[0]);

	        // Create directory path to HTTP Cache, if path does not already exist
	        File cachePath = new File("/tmp/ami325/assignment1/");
				if (!cachePath.exists()) {
					cachePath.mkdirs();
				}

	        // Check if file exists in the cache
	        boolean fileCheck = new File("/tmp/ami325/assignment1/" + cleanURL + ".txt").exists();
	        


	        /*****************************************************************************
	         *						Case 1: Data exists in cache 
	         *****************************************************************************/
	        if (fileCheck) {

	        	// Pull data from deserialized Map
	        	Map<String, Object> deserializedMap = deserialize(cleanURL);
		        	String responseCode = getResponseCode(deserializedMap);
		        	Long date = getDate(deserializedMap);
		        	Long expires = getExpires(deserializedMap);
		        	Long lastModified = getLastModified(deserializedMap);
		        	Long maxAge = getMaxAge(deserializedMap);
		        	String lastModifiedString = (String) deserializedMap.get("Last-Modified-String");
		        	String dateString = (String) deserializedMap.get("Date-String");

	        	// Check that data is not expired ****************************************
        		if (maxAge != null) {
        			
        			// A maximum age is defined; check that cache entry is still fresh
        			if (maxAge != 0) {
        				
        				// If cache entry is stale according to maxAge value
        				Long currentTime = System.currentTimeMillis();
        				if ((currentTime - date) >= maxAge) {
        					
        					URLConnection connection;

        					// Send an if-modified-since verification using lastModified or Date
	        				if (lastModified != null && lastModified != 0) {
	        					connection = url.openConnection();
	        					connection.setRequestProperty("If-Modified-Since", lastModifiedString);
	      
	        				} else {
	        					connection = url.openConnection();
	        					connection.setRequestProperty("If-Modified-Since", dateString);
	        				}

	        				// Interpret + handle if-Modified-Since response
	        				interpretIMSResponse(connection, url);

	        			// If cache entry is NOT stale
        				} else {
        					
        					System.out.println("***** Serving from the cache – start *****");
        					
        						readFromCache(cleanURL + ".txt"); // read cached data to output
        					
        					System.out.println("***** Serving from the cache – end *****");
        				}
        			}
        			
        		// If expires value exists; check that cache entry is still fresh
        		} else if (expires != null) {

        			// If cache entry is stale according to expires value
        			Long currentTime = System.currentTimeMillis();
        			if(currentTime > expires) {
        				
        				URLConnection connection;

        				// Send an if-modified-since verification using lastModified or Date
        				if (lastModified != null && lastModified != 0) {
        					connection = url.openConnection();
        					connection.setRequestProperty("If-Modified-Since", lastModifiedString);
      
        				} else {
        					connection = url.openConnection();
        					connection.setRequestProperty("If-Modified-Since", dateString);
        				}

        				// Interpret + handle if-Modified-Since response
        				interpretIMSResponse(connection, url);

        			// If cache entry is NOT stale
        			} else {
        				
        				System.out.println("***** Serving from the cache – start *****");
        				
        					readFromCache(cleanURL + ".txt"); // read cached data to output
        				
        				System.out.println("***** Serving from the cache – end *****");
        			}
        		}



			/*****************************************************************************
	         *						Case 2: Data is NOT in cache 
	         *****************************************************************************/
	        } else {

	        	URLConnection connection = url.openConnection();

	        	// Write incoming data from server to output
	        	System.out.println("***** Serving from the source – start *****");
	        		
	        		writeToOutput(connection, url); // Writes to output
	        	
	        	System.out.println("***** Serving from the source – end *****");

        		// Save data to cache ONLY IF either Max-Age or Expires exists
	        	Long expires = connection.getExpiration();
	        	String maxAgeString = connection.getHeaderField("Cache-Control");
	        	Long maxAge = getMaxAge(maxAgeString);
	        	Long date = connection.getDate();
		        
		        // Ensure a Max-Age value > 0 exists 
		        if (maxAge != null) {
		        	if (maxAge != 0)
			        	saveToCache(connection, url); // Save to cache because valid Max-Age exists
		        	
		        // Ensure an Expires value exists
	        	} else if (expires != null && expires != 0) {
	        		if (expires > date)
			        	saveToCache(connection, url); // Save to cache because valid Expires exists
	        	}
	
	        }
        	        		
		} catch (IOException e) {
			e.printStackTrace();
		}

	}











	/*****************************************************************************
	 								   URL CLEANER
	 *****************************************************************************/
	public static String urlCleaner(String s) {
		
		int index;
		
		// Remove "http" "https"
		if ((index = s.indexOf("https")) != -1) {
			s = s.substring(index + 5, s.length());
		
		} else if ((index = s.indexOf("http")) != -1) {
			s = s.substring(index + 4, s.length());
		}

		// Remove "www"
		if ((index = s.indexOf("www")) != -1) {
			s = s.substring(index + 3, s.length());
		}
		
		// Remove unsafe characters
		s = s.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
		
		return s;
	}


	/*****************************************************************************
	 						WRITE HEADER + DATA TO OUTPUT
	 *****************************************************************************/
	public static void writeToOutput(URLConnection c, URL url) {
		
		try {
			
			c = url.openConnection();
			System.out.println("Response-Code: " + c.getHeaderField(0));

			System.out.println("Content-Encoding:" + c.getContentEncoding());
			System.out.println("Content-Length:" + c.getContentLength());
			System.out.println("Content-Type:" + c.getContentType());
			
			System.out.println("Date: " + c.getHeaderField("Date"));
			System.out.println("Transfer-Encoding: " + c.getHeaderField("Transfer-Encoding"));
			System.out.println("Transfer-Length: " + c.getHeaderField("Transfer-Length"));
			System.out.println("Expires: " + c.getHeaderField("Expires"));
			System.out.println("Last-Modified: " + c.getHeaderField("Last-Modified"));
			System.out.println("If-Modified-Since: " + c.getIfModifiedSince());
			System.out.println("Cache-Control:"  + c.getHeaderField("Cache-Control"));
			System.out.println("Connection: " + c.getHeaderField("Connection"));
			System.out.println();

			InputStream input = c.getInputStream();
			byte[] buffer = new byte[4096];
			int n = - 1;

			while ( (n = input.read(buffer)) != -1)
			{
			    if (n > 0)
			    {
			        System.out.write(buffer, 0, n);
			    }
			}		

		} catch (IOException e) {
			e.printStackTrace();
		}
						
	}


	public static void interpretIMSResponse(URLConnection connection, URL url) {

		String cleanURL = urlCleaner(url.toString());

		String newResponseCode = connection.getHeaderField(0);
		newResponseCode = getResponseCode(newResponseCode);

		// If 304 Not Modified is returned
		if(newResponseCode.compareTo("304") == 0) {
			
			System.out.println("***** Serving from the cache – start *****");
			
				readFromCache(cleanURL + ".txt"); // Read cached data to output
			
			System.out.println("***** Serving from the cache – end *****");
		
		// If 200 is returned, data has been modified
		} else if (newResponseCode.compareTo("200") == 0) {
			
			System.out.println("***** Serving from the source – start *****");
			
				writeToOutput(connection, url); // Writes to output

			System.out.println("***** Serving from the source – end *****");
			
			//Cache server response
			saveToCache(connection, url);

		} 

	}


	/*****************************************************************************
	 						SAVE DATA TO CACHE FILES
	 *****************************************************************************/
	public static void saveToCache(URLConnection connection, URL url) {
		
		try {
			
			String cleanURL = urlCleaner(url.toString());
			
			//Cache server response
			// Create .txt, .ser files
			FileOutputStream srcFile = new FileOutputStream("/tmp/ami325/assignment1/" + cleanURL + ".txt");
			FileOutputStream serFile = new FileOutputStream("/tmp/ami325/assignment1/" + cleanURL + ".ser");
				writeToFile(connection, srcFile, url); // Save data to cache
			
			// Fill map object with header information
	    	Map<String, Object> headerMap = new HashMap<String, Object>();
				headerMap.put("Response-Code", connection.getHeaderField(0));
				headerMap.put("Date", connection.getDate()); //System.currentTimeMillis() in case .getDate==null
				headerMap.put("Expires", connection.getExpiration());
				headerMap.put("Last-Modified", connection.getLastModified());
				headerMap.put("Cache-Control", connection.getHeaderField("Cache-Control"));
				headerMap.put("Last-Modified-String", connection.getHeaderField("Last-Modified"));
				headerMap.put("Date-String", connection.getHeaderField("Date"));

			// Write headerMap Object to .ser file
			ObjectOutputStream serObjectOut = new ObjectOutputStream(serFile);
				serObjectOut.writeObject(headerMap);
				serObjectOut.close();
				serFile.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	/*****************************************************************************
	 						WRITE HEADER + DATA TO TXT FILE
	 *****************************************************************************/
	public static void writeToFile(URLConnection c, FileOutputStream f, URL url) {
		try {
			
			c = url.openConnection();
			
			// Sends Header bytes
			f.write(("Response-Code: " + c.getHeaderField(0) + "\n").getBytes());
			
			f.write(("Content-Encoding:" + c.getContentEncoding() + "\n").getBytes());
			f.write(("Content-Length:" + c.getContentLength() + "\n").getBytes());
			f.write(("Content-Type:" + c.getContentType() + "\n").getBytes());
			
			f.write(("Date:" + c.getHeaderField("Date") + "\n").getBytes());
			f.write(("Transfer-Encoding:" + c.getHeaderField("Transfer-Encoding") + "\n").getBytes());
			f.write(("Transfer-Length:" + c.getHeaderField("Transfer-Length") + "\n").getBytes());
			f.write(("Expires:" + c.getHeaderField("Expires") + "\n").getBytes());
			f.write(("Last-Modified: " + c.getHeaderField("Last-Modified") + "\n").getBytes());
			f.write(("If-Modified-Since: " + c.getIfModifiedSince() + "\n").getBytes());
			f.write(("Cache-Control:" + c.getHeaderField("Cache-Control") + "\n").getBytes());
			f.write(("Connection:" + c.getHeaderField("Connection") + "\n").getBytes());
			f.write(("\n").getBytes());

			// Sends buffered Content bytes to .txt File
			InputStream srcIn = c.getInputStream();
			
			byte[] buffer = new byte[4096];
			int n = - 1;

			while ( (n = srcIn.read(buffer)) != -1)
			{
			    if (n > 0)
			    {
			    	f.write(buffer, 0, n); // Writes Data to source file
			    }	
			}

		} catch (IOException e) {
			e.printStackTrace();
		}	

	}


	/*****************************************************************************
	 							READ FROM CACHE FILE
	 *****************************************************************************/
	public static void readFromCache(String fileName) {
		
		try{

            InputStream ips = new FileInputStream("/tmp/ami325/assignment1/" + fileName); 
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String line;
            
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }

            br.close(); 
       
        } catch (IOException e) {
			e.printStackTrace();
		}	

	}


	/*****************************************************************************
	 							DESERIALIZE MAP
	 *****************************************************************************/
	@SuppressWarnings("unchecked")
	public static Map<String, Object> deserialize(String url) {
		
		try {
	        	FileInputStream serFileIn = new FileInputStream("/tmp/ami325/assignment1/" + url + ".ser");
	        	ObjectInputStream serObjectIn = new ObjectInputStream(serFileIn);
	        	Map<String, Object> serObject = (Map<String, Object>) serObjectIn.readObject();
	        	serObjectIn.close();
	        	serFileIn.close();
	        	return serObject;
	    
	        } catch (IOException i) {
        		i.printStackTrace();
        		return null;
        
        	} catch (ClassNotFoundException c) {
        		System.out.println("Class not found");
        		c.printStackTrace();
        		return null;
      		}

	}


	/*****************************************************************************
	 							PROCESS MAP OBJECTS
	 *****************************************************************************/
	public static String getResponseCode(Map<String, Object> m) {
		
		if (m.get("Response-Code") != null) {
			String s = (m.get("Response-Code")).toString();
			s = s.substring(9,12);
			return s;
		
		} else {
			return null;
		}

	}
	
	public static String getResponseCode(String s) {
		
		if (s != null) {
			s = s.substring(9,12);
			return s;
		
		} else {
			return null;
		}

	}

	// Returns miliseconds
	public static Long getDate(Map<String, Object> m) {
		
		if (m.get("Date") != null) {
			return (Long) m.get("Date");
		
		} else {
			return null;
		}

	}
	
	// Returns miliseconds
	public static Long getExpires(Map<String, Object> m) {
		
		if (m.get("Expires") != null) {
			return (Long) m.get("Expires");
		
		} else {
			return null;
		}

	}

	// Returns miliseconds
	public static Long getLastModified(Map<String, Object> m) {
		
		if (m.get("Last-Modified") != null) {
			return (Long) m.get("Last-Modified");	
		
		} else {
			return null;
		}

	}

	// Returns miliseconds
	public static Long getMaxAge(Map<String, Object> m) {
		
		if (m.get("Cache-Control") != null) {
			String s = (m.get("Cache-Control")).toString();
			int index = s.indexOf("max-age=");
			
			if (index != -1) {
				index += 8;
			
				boolean isDigit = true;
				int n = 0;
				
				while (isDigit) {
					if ((index + n) < s.length()) {
						isDigit = Character.isDigit(s.charAt(index + n));
					
					} else {
						isDigit = false;
					}
					
					n++;
				}
				
				s = s.substring(index,index+(n-1));
				return Long.valueOf(s)*1000;
			
			} else {
				return null;
			}

		} else {
			return null;
		}

	}

	// Returns miliseconds
	public static Long getMaxAge(String s) {
		
		if (s != null) {
			int index = s.indexOf("max-age=");
			
			if (index != -1) {
				index += 8;
			
				boolean isDigit = true;
				int n = 0;
				
				while (isDigit) {
					if ((index + n) < s.length()) {
						isDigit = Character.isDigit(s.charAt(index + n));
					
					} else {
						isDigit = false;
					}
					
					n++;
				}
				
				s = s.substring(index,index+(n-1));
				return Long.valueOf(s)*1000;
			
			} else {
				return null;
			}

		} else {
			return null;
		}
		
	}
        	
}